/*
 * LIGHT
 * Copyright © 2016+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <sstream>
#include <Light.hpp>

namespace glt
{

    void Light::shader_changed (const Shader_Program & active_shader_program)
    {
        if (index >= 0)
        {
            std::ostringstream  position_identifier;
            std::ostringstream     color_identifier;
            std::ostringstream intensity_identifier;

            position_identifier  << "lights[" << index << "].position";
            color_identifier     << "lights[" << index << "].color";
            intensity_identifier << "lights[" << index << "].intensity";

            if (visible)
            {
                const Vector4 & position4 = get_transformation ()[3];
                const Vector3   position3(position4[0], position4[1], position4[2]);

                active_shader_program.set_uniform_value (active_shader_program.get_uniform_id ( position_identifier.str ().c_str ()), position3);
                active_shader_program.set_uniform_value (active_shader_program.get_uniform_id (    color_identifier.str ().c_str ()), color    );
                active_shader_program.set_uniform_value (active_shader_program.get_uniform_id (intensity_identifier.str ().c_str ()), intensity);
            }
            else
            {
                active_shader_program.set_uniform_value (active_shader_program.get_uniform_id (intensity_identifier.str ().c_str ()), 0.f);
            }
        }
    }

}
