/*
 * VERTEX BUFFER OBJECT
 * Copyright © 2016+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <Vertex_Buffer_Object.hpp>

namespace glt
{

    void Vertex_Buffer_Object::create (const void * data, size_t size)
    {
        assert(data && size);

      //clear_gl_error ();

        glGenBuffers (1, &vbo_id);

        bind ();

        glBufferData (target, size, data, GL_STATIC_DRAW);

        error = glGetError ();

        assert(error == GL_NO_ERROR);

        unbind ();
    }

}
