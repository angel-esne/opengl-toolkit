/*
 * OPENGL
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#pragma warning(disable: 4251)                  // https://github.com/cginternals/glbinding/issues/141

#include <glad/glad.h>

namespace glt
{

    bool initialize_opengl_extensions ()
    {
		return gladLoadGL () != 0;
    }

}
