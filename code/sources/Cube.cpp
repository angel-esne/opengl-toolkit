/*
 * CUBE
 * Copyright © 2016+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <Cube.hpp>

namespace glt
{

    const GLfloat Cube::coordinates[] =
    {
       -1.f, +1.f, +1.f,        // 0
       +1.f, +1.f, +1.f,        // 1
       +1.f, -1.f, +1.f,        // 2
       -1.f, -1.f, +1.f,        // 3
       -1.f, +1.f, -1.f,        // 4
       -1.f, +1.f, +1.f,        // 5
       -1.f, -1.f, +1.f,        // 6
       -1.f, -1.f, -1.f,        // 7
       +1.f, +1.f, -1.f,        // 8
       -1.f, +1.f, -1.f,        // 9
       -1.f, -1.f, -1.f,        // 10
       +1.f, -1.f, -1.f,        // 11
       +1.f, +1.f, +1.f,        // 12
       +1.f, +1.f, -1.f,        // 13
       +1.f, -1.f, -1.f,        // 14
       +1.f, -1.f, +1.f,        // 15
       -1.f, -1.f, +1.f,        // 16
       +1.f, -1.f, +1.f,        // 17
       +1.f, -1.f, -1.f,        // 18
       -1.f, -1.f, -1.f,        // 19
       -1.f, +1.f, -1.f,        // 20
       +1.f, +1.f, -1.f,        // 21
       +1.f, +1.f, +1.f,        // 22
       -1.f, +1.f, +1.f,        // 23
    };

    const GLfloat Cube::normals[] =
    {
        0.f,  0.f, +1.f,        // 0
        0.f,  0.f, +1.f,        // 1
        0.f,  0.f, +1.f,        // 2
        0.f,  0.f, +1.f,        // 3
       -1.f,  0.f,  0.f,        // 4
       -1.f,  0.f,  0.f,        // 5
       -1.f,  0.f,  0.f,        // 6
       -1.f,  0.f,  0.f,        // 7
        0.f,  0.f, -1.f,        // 8
        0.f,  0.f, -1.f,        // 9
        0.f,  0.f, -1.f,        // 10
        0.f,  0.f, -1.f,        // 11
       +1.f,  0.f,  0.f,        // 12
       +1.f,  0.f,  0.f,        // 13
       +1.f,  0.f,  0.f,        // 14
       +1.f,  0.f,  0.f,        // 15
        0.f, -1.f,  0.f,        // 16
        0.f, -1.f,  0.f,        // 17
        0.f, -1.f,  0.f,        // 18
        0.f, -1.f,  0.f,        // 19
        0.f, +1.f,  0.f,        // 20
        0.f, +1.f,  0.f,        // 21
        0.f, +1.f,  0.f,        // 22
        0.f, +1.f,  0.f,        // 23
    };

    const GLfloat Cube::texture_uvs[] =
    {
        0.f, 0.f,               // 0
        1.f, 0.f,               // 1
        1.f, 1.f,               // 2
        0.f, 1.f,               // 3
        0.f, 0.f,               // 4
        1.f, 0.f,               // 5
        1.f, 1.f,               // 6
        0.f, 1.f,               // 7
        0.f, 0.f,               // 8
        1.f, 0.f,               // 9
        1.f, 1.f,               // 10
        0.f, 1.f,               // 11
        0.f, 0.f,               // 12
        1.f, 0.f,               // 13
        1.f, 1.f,               // 14
        0.f, 1.f,               // 15
        0.f, 0.f,               // 16
        1.f, 0.f,               // 17
        1.f, 1.f,               // 18
        0.f, 1.f,               // 19
        0.f, 0.f,               // 20
        1.f, 0.f,               // 21
        1.f, 1.f,               // 22
        0.f, 1.f,               // 23
    };

    const GLubyte Cube::indices[] =
    {
        1,  0,  3,              // front
        1,  3,  2,
        5,  4,  7,              // left
        5,  7,  6,
        9,  8,  11,             // back
        9,  11, 10,
        13, 12, 15,             // right
        13, 15, 14,
        17, 16, 19,             // top
        17, 19, 18,
        20, 23, 22,             // bottom
        20, 22, 21,
    };

    Cube::Cube()
    {
        std::shared_ptr< Vertex_Buffer_Object > vbo_coordinates(new Vertex_Buffer_Object(coordinates, sizeof(coordinates)));
        std::shared_ptr< Vertex_Buffer_Object > vbo_normals    (new Vertex_Buffer_Object(normals,     sizeof(normals    )));
        std::shared_ptr< Vertex_Buffer_Object > vbo_indices    (new Vertex_Buffer_Object(indices,     sizeof(indices    ), Vertex_Buffer_Object::ELEMENT_ARRAY_BUFFER));

        vao.reset
        (
            new Vertex_Array_Object
            (
                {
                    { vbo_coordinates, Vertex_Attribute::COORDINATES, 3, GL_FLOAT },
                    { vbo_normals,     Vertex_Attribute::NORMALS,     3, GL_FLOAT }
                },
                vbo_indices
            )
        );

        set_primitive_type (GL_TRIANGLES    );
        set_indices_type   (GL_UNSIGNED_BYTE);
        set_vertices_count (sizeof(indices) );
    }

}
