/*
 * RENDER NODE
 * Copyright © 2016+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <Model.hpp>
#include <Light.hpp>
#include <Drawable.hpp>
#include <Render_Node.hpp>

namespace glt
{

    bool Render_Node::add (const std::string & name, std::shared_ptr< Node > node)
    {
        // No puede haber varios nodes con el mismo nombre:

        if (nodes.find (name) != nodes.end ())
        {
            assert(false);                      // Cuando está activa la configuración debug se resalta el error
            return false;
        }

        // Se establece el parent del node y se agrega al mapa de nodes:

        node->set_renderer (this);

        nodes[name] = node;

        // Si es un modelo 3D, se añaden sus drawables a la lista de drawables estructurada para optimizar el render:

        Model * model = dynamic_cast< Model * >(node.get ());

        if (model != nullptr)
        {
            for (const Model::Piece & piece : model->get_pieces ())
            {
                drawable_list[piece.material->get_shader_program ()][piece.material.get ()][node.get ()].push_back (piece.drawable.get ());
            }
        }

        // Si es una cámara, se establece como cámara por defecto si no hay alguna:

        if (active_camera == nullptr)
        {
            active_camera =  dynamic_cast< Camera * >(node.get ());         // Se queda nulo si no es una cámara
        }

        // Si es una luz, se le asigna un índice en la escena:

        Light * light = dynamic_cast< Light * >(node.get ());

        if (light != nullptr)
        {
            light->set_light_index (number_of_lights < max_number_of_lights ? number_of_lights : -1);

            number_of_lights++;
        }

        // Si el node modifica alguna variable uniforme de los shaders, se le añade a una lista:

        if (node->changes_shaders ())
        {
            shader_update_list.push_back (node.get ());
        }

        return true;
    }

    Node * Render_Node::get (const std::string & name)
    {
        Node_Map::iterator node = nodes.find (name);

        if (node != nodes.end ())
        {
            return node->second.get ();
        }

        return nullptr;
    }

    bool Render_Node::set_active_camera (const std::string & name)
    {
        Camera * camera = dynamic_cast< Camera * >(get (name));

        if (camera)
        {
            active_camera = camera;

            return true;
        }

        return false;
    }

    bool Render_Node::render ()
    {
        if (active_camera == nullptr)
        {
            assert(false);                  // Se resalta el error cuando está activa la configuración debug
            return false;
        }

        glEnable (GL_DEPTH_TEST);
        glEnable (GL_CULL_FACE );

        const Matrix44 view_matrix = active_camera->get_inverse_total_transformation ();

        // Para cada shader se recorren los materiales que lo usan:

        for (auto & drawables_by_shader : drawable_list)
        {
            // Se pone en uso el shader_program que usan cierto grupo de materiales y drawables:

            Shader_Program * active_shader_program = drawables_by_shader.first;

            active_shader_program->use ();

            // Se ajusta alguna variable uniforme general:

            active_shader_program->set_uniform_value (active_shader_program->get_uniform_id ("projection_matrix"), active_camera->get_projection_matrix ());

            // Se avisa a los nodes que necesitan ajustar alguna variable uniforme:

            for (auto & node : shader_update_list)
            {
                node->shader_changed (*active_shader_program);
            }

            // Para cada material se recorren los drawables que lo usan:

            for (auto & drawables_by_material : drawables_by_shader.second)
            {
                // Se establece el material activo (puede modificar algunas variables uniformes):

                Material * material = drawables_by_material.first;

                material->use ();

                // Para cada grupo de drawables que usan cierto material se recorren los drawables de un mismo node:

                for (auto & drawables_by_object : drawables_by_material.second)
                {
                    if (drawables_by_object.first->is_visible ())
                    {
                        // Se calculan las matrices de transformación del objeto y se actualizan las variables uniformes correspondientes:

                        const Matrix44 model_view_matrix        = view_matrix * drawables_by_object.first->get_total_transformation ();
                        const Matrix44 model_view_normal_matrix = transpose (inverse (model_view_matrix));

                        active_shader_program->set_uniform_value (active_shader_program->get_uniform_id ("model_view_matrix"       ), model_view_matrix       );
                        active_shader_program->set_uniform_value (active_shader_program->get_uniform_id ("model_view_normal_matrix"), model_view_normal_matrix);

                        // Se dibujan los drawables de un mismo node que comparten cierto material:

                        for (auto & drawable : drawables_by_object.second)
                        {
                            drawable->draw ();
                        }
                    }
                }
            }
        }

        return true;
    }

}
