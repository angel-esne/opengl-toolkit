/*
 * MATERIAL
 * Copyright © 2016+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <ciso646>
#include <Material.hpp>
#include <Vertex_Shader.hpp>
#include <Fragment_Shader.hpp>

using namespace std;

namespace glt
{

    static const string default_vertex_shader_code =

        "#version 330\n"
        ""
        "struct Light"
        "{"
        "   vec3  position;"
        "   vec3  color;"
        "   float intensity;"
        "};"
        ""
        "uniform mat4  projection_matrix;"
        "uniform mat4  model_view_matrix;"
        "uniform mat4  model_view_normal_matrix;"
        ""
        "uniform Light lights[3];"
        ""
        "uniform vec3  material_color;"
        ""
        "layout (location = 0) in vec3 vertex_position;"
        "layout (location = 1) in vec3 vertex_normal;"
        ""
        "out vec3 front_color;"
        ""
        "void main()"
        "{"
        "    vec4  normal          = normalize (model_view_normal_matrix * vec4(vertex_normal, 1.0));"
        "    vec4  light_direction = vec4(normalize (lights[0].position), 1.0);"
        "    float intensity       = max (dot (normal, light_direction), 0.0) * lights[0].intensity;"
        ""
        "    front_color = material_color * lights[0].color * intensity;"
        "    gl_Position = projection_matrix * model_view_matrix * vec4(vertex_position, 1.0);"
        "}";

    static const string default_fragment_shader_code =

        "#version 330\n"
        ""
        "in  vec3 front_color;"
        "out vec4 fragment_color;"
        ""
        "void main()"
        "{"
        "    fragment_color = vec4(front_color, 1.0);"
        "}";

    unsigned Material::instance_count = 0;

    shared_ptr< Material > Material:: default_material ()
    {
        static shared_ptr< Shader_Program > shader_program;

        if (not shader_program)
        {
            shader_program = make_shared< Shader_Program > ();

            shader_program->attach (  Vertex_Shader(Shader::Source_Code::from_string (  default_vertex_shader_code)));
            shader_program->attach (Fragment_Shader(Shader::Source_Code::from_string (default_fragment_shader_code)));
            shader_program->link   ();
        }

        shared_ptr< Material > default_material;

        default_material.reset (new Material("default_material", shader_program));

        default_material->set ("material_color", Vector3(.5f, .5f, .5f));

        return default_material;
    }

    bool Material::set (const char * identifier, const GLint value)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::INT);

        if (uniform)
        {
            uniform->value.data.gl_int = value;

            return true;
        }

        return false;
    }

    bool Material::set (const char * identifier, const GLuint value)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::UINT);

        if (uniform)
        {
            uniform->value.data.gl_uint = value;

            return true;
        }

        return false;
    }

    bool Material::set (const char * identifier, const GLfloat value)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::FLOAT);

        if (uniform)
        {
            uniform->value.data.gl_float = value;

            return true;
        }

        return false;
    }

    bool Material::set (const char * identifier, const Vector2 & vector2)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::VECTOR2);

        if (uniform)
        {
            uniform->value.data.vector2[0] = vector2[0];
            uniform->value.data.vector2[1] = vector2[1];

            return true;
        }

        return false;
    }

    bool Material::set (const char * identifier, const Vector3 & vector3)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::VECTOR3);

        if (uniform)
        {
            uniform->value.data.vector3[0] = vector3[0];
            uniform->value.data.vector3[1] = vector3[1];
            uniform->value.data.vector3[2] = vector3[2];

            return true;
        }

        return false;
    }

    bool Material::set (const char * identifier, const Vector4 & vector4)
    {
        Uniform * uniform = allocate_uniform (identifier, Var::Type::VECTOR4);

        if (uniform)
        {
            uniform->value.data.vector4[0] = vector4[0];
            uniform->value.data.vector4[1] = vector4[1];
            uniform->value.data.vector4[2] = vector4[2];
            uniform->value.data.vector4[3] = vector4[3];

            return true;
        }

        return false;
    }

    Material::Uniform * Material::allocate_uniform (const char * identifier, Var::Type type)
    {
        Uniform uniform;

        uniform.location = shader_program->get_uniform_id (identifier);

        if (uniform.location >= 0)
        {
            uniform.value.type = type;

            uniforms.push_back (uniform);

            return &uniforms.back ();
        }

        return nullptr;
    }

    void Material::use ()
    {
        shader_program->use ();

        for (auto & uniform : uniforms)
        {
            switch (uniform.value.type)
            {
                case Var::Type::INT:     shader_program->set_uniform_value (uniform.location, uniform.value.data.gl_int  ); break;
                case Var::Type::UINT:    shader_program->set_uniform_value (uniform.location, uniform.value.data.gl_uint ); break;
                case Var::Type::FLOAT:   shader_program->set_uniform_value (uniform.location, uniform.value.data.gl_float); break;
                case Var::Type::VECTOR2: shader_program->set_uniform_value (uniform.location, uniform.value.data.vector2 ); break;
                case Var::Type::VECTOR3: shader_program->set_uniform_value (uniform.location, uniform.value.data.vector3 ); break;
                case Var::Type::VECTOR4: shader_program->set_uniform_value (uniform.location, uniform.value.data.vector4 ); break;
                default:
                {
                    assert(false);
                }
            }
        }
    }

}
