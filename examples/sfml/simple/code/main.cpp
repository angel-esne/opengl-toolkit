/*
 * Started by �ngel on January 2018
 *
 * This is free software released into the public domain.
 *
 * angel.rodriguez@esne.edu
 */

#include <Cube.hpp>
#include <Light.hpp>
#include <Model.hpp>
#include <OpenGL.hpp>
#include <Model_Obj.hpp>
#include <Render_Node.hpp>
#include <SFML/Window.hpp>

using namespace glt;
using namespace std;

namespace
{

    shared_ptr< Render_Node > create_scene (int number_of_arguments, char * arguments[])
    {
        // Se crean los elementos (nodos) y la escena a la que se a�adir�n:

        shared_ptr< Render_Node > scene (new Render_Node);
        shared_ptr< Model       > model (new Model_Obj(number_of_arguments > 1 ? arguments[1] : "../../../assets/bunny.obj"));
        shared_ptr< Model       > cube1 (new Model);
        shared_ptr< Model       > cube2 (new Model);
        shared_ptr< Camera      > camera(new Camera(20.f, 1.f, 50.f, 1.f));
        shared_ptr< Light       > light (new Light);

        // Es necesario a�adir las mallas a los modelos antes de a�adir los modelos a la escena:

        shared_ptr< Drawable > cubes_mesh = shared_ptr < Drawable >(new Cube);

        cube1->add (cubes_mesh, Material::default_material ());
        cube2->add (cubes_mesh, Material::default_material ());

        // Se establece el modelo cargado desde un archivo como parent del cubo:

        cube1->set_parent (model.get ());
        cube2->set_parent (model.get ());

        // Se a�aden los nodos a la escena:

        scene->add ("model" , model );
        scene->add ("cube1" , cube1 );
        scene->add ("cube2" , cube2 );
        scene->add ("camera", camera);
        scene->add ("light" , light );

        return scene;
    }

    void set_model_color (glt::Node * node, const Vector3 & color)
    {
        auto * model = dynamic_cast< Model * >(node);

        if (model)
        {
            for (auto & piece : model->get_pieces ())
            {
                piece.material->set ("material_color", color);
            }
        }
    }

    void configure_scene (Render_Node & scene)
    {
        set_model_color (scene["cube1"], Vector3(.5f, .0f, .0f));
        set_model_color (scene["model"], Vector3(.0f, .7f, .0f));
        set_model_color (scene["cube2"], Vector3(.0f, .0f, .5f));

        scene["light"]->translate (Vector3(10.f, 10.f, 10.f));
        scene["model"]->translate (Vector3( 0.f,  0.f, -5.f));
        scene["model"]->scale     (1.75f);
        scene["cube1"]->scale     (0.25f);
        scene["cube1"]->translate (Vector3( 6.f,  0.f,  0.f));
        scene["cube2"]->scale     (0.5f);
        scene["cube2"]->translate (Vector3(-3.f,  0.f,  0.f));
    }

    void reset_viewport (const sf::Window & window, Render_Node & scene)
    {
        GLsizei width  = GLsizei(window.getSize ().x);
        GLsizei height = GLsizei(window.getSize ().y);

        scene.get_active_camera ()->set_aspect_ratio (float(width) / height);

        glViewport (0, 0, width, height);
    }

}

int main (int number_of_arguments, char * arguments[])
{
    // Se crea la ventana y el contexto de OpenGL asociado a ella:

    sf::Window window
    (
        sf::VideoMode(800, 600),
        "OpenGL Toolkit Simple Example",
        sf::Style::Default,
        sf::ContextSettings(24, 0, 0, 3, 2, sf::ContextSettings::Core)      // Se usa OpenGL 3.2 core profile
    );

    // Se determinan las caracter�sticas de OpenGL disponibles en la m�quina:

	if (!glt::initialize_opengl_extensions())
	{
		exit (-1);
	}

    // Se activa la sincronizaci�n vertical:

    window.setVerticalSyncEnabled (true);

    // Se crea y se configura la escena:

    shared_ptr< Render_Node > scene = create_scene (number_of_arguments, arguments);

    Node * model = scene->get ("model");

    configure_scene (*scene);

    // Se inicializan algunos elementos de OpenGL:

    reset_viewport (window, *scene);

    glClearColor (0.2f, 0.2f, 0.2f, 1.f);

    // Bucle principal:

    bool running = true;

    do
    {
        sf::Event event;

        while (window.pollEvent (event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                {
                    running = false;
                    break;
                }

                case sf::Event::Resized:
                {
                    reset_viewport (window, *scene);
                    break;
                }
            }
        }

        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        model->rotate_around_y (0.01f);

        scene->render ();

        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
